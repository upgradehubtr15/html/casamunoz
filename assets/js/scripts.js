$(function(){

    $('#header').load("header.html");

    $(".acetrnt-toggle").click(function(){
        $('body').toggleClass("open-menu");
      });

    $(".left").mouseover(function(){
        $("#leftArrow").css("display", "block");
    });

    $(".left").mousemove(function(e){
        $('#leftArrow').css({
            left:  e.pageX,
            top:   e.pageY-25,
        });
    });

    $(".left").mouseout(function(){
        $("#leftArrow").css("display", "none");
    });

    $(".right").mouseover(function(){
        $("#rightArrow").css("display", "block");
    });
    
    $(".right").mousemove(function(e){
        $('#rightArrow').css({
            left:  e.pageX-25,
            top:   e.pageY-25,
        });
    });

    $(".right").mouseout(function(){
        $("#rightArrow").css("display", "none");
    });
});

